﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VrticApp2
{
    public partial class PromptDlg : Form
    {
        VrticDBManager _dBManager;
        public PromptDlg(ref VrticDBManager dBManager, string txt)
        {
            InitializeComponent();
            _dBManager = dBManager;
            txtLbl.Text = txt;
        }

        private void izbrisiBtn_Click(object sender, EventArgs e)
        {
            _dBManager.DeleteAllPolaznici();
            this.Close();
        }

        private void odustaniBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
