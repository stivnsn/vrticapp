﻿namespace VrticApp2
{
    partial class WarningDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtLbl = new System.Windows.Forms.Label();
            this.ureduBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtLbl
            // 
            this.txtLbl.AutoSize = true;
            this.txtLbl.Location = new System.Drawing.Point(12, 31);
            this.txtLbl.Name = "txtLbl";
            this.txtLbl.Size = new System.Drawing.Size(140, 17);
            this.txtLbl.TabIndex = 0;
            this.txtLbl.Text = "Tekst nije postavljen!";
            // 
            // ureduBtn
            // 
            this.ureduBtn.Location = new System.Drawing.Point(115, 94);
            this.ureduBtn.Name = "ureduBtn";
            this.ureduBtn.Size = new System.Drawing.Size(231, 52);
            this.ureduBtn.TabIndex = 1;
            this.ureduBtn.Text = "U redu";
            this.ureduBtn.UseVisualStyleBackColor = true;
            this.ureduBtn.Click += new System.EventHandler(this.ureduBtn_Click);
            // 
            // WarningDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(491, 158);
            this.Controls.Add(this.ureduBtn);
            this.Controls.Add(this.txtLbl);
            this.Name = "WarningDlg";
            this.Text = "WarningDlg";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txtLbl;
        private System.Windows.Forms.Button ureduBtn;
    }
}