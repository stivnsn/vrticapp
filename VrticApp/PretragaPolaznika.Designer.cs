﻿namespace VrticApp2
{
    partial class PretragaDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.polazniciDgv = new System.Windows.Forms.DataGridView();
            this.pretragaTB = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.datotekaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.zatvoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.upravljanjeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uveziKontakteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izveziPodatkeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.izbrišiSvePodatkeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pomoćToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.oProgramuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.pretragaLbl = new System.Windows.Forms.Label();
            this.vrticLbl = new System.Windows.Forms.Label();
            this.skupinaLbl = new System.Windows.Forms.Label();
            this.specificnostLbl = new System.Windows.Forms.Label();
            this.programLbl = new System.Windows.Forms.Label();
            this.vrticCLB = new System.Windows.Forms.CheckedListBox();
            this.specificnostCLB = new System.Windows.Forms.CheckedListBox();
            this.programCLB = new System.Windows.Forms.CheckedListBox();
            this.skupinaCLB = new System.Windows.Forms.CheckedListBox();
            this.resetFilterBtn = new System.Windows.Forms.Button();
            this.pronadenoLbl = new System.Windows.Forms.Label();
            this.brojRezultataLbl = new System.Windows.Forms.Label();
            this.upisLbl = new System.Windows.Forms.Label();
            this.spolLbl = new System.Windows.Forms.Label();
            this.godineLbl = new System.Windows.Forms.Label();
            this.upisCLB = new System.Windows.Forms.CheckedListBox();
            this.spolCLB = new System.Windows.Forms.CheckedListBox();
            this.godineCLB = new System.Windows.Forms.CheckedListBox();
            this.dokumentacijaLbl = new System.Windows.Forms.Label();
            this.dokumentacijaCLB = new System.Windows.Forms.CheckedListBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbSuradnici = new System.Windows.Forms.CheckedListBox();
            this.lblSuradnici = new System.Windows.Forms.Label();
            this.lbObveznik = new System.Windows.Forms.CheckedListBox();
            this.lblObveznk = new System.Windows.Forms.Label();
            this.lbDokVrtic = new System.Windows.Forms.CheckedListBox();
            this.lblDokVrtic = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.polazniciDgv)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // polazniciDgv
            // 
            this.polazniciDgv.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.polazniciDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.polazniciDgv.Location = new System.Drawing.Point(145, 78);
            this.polazniciDgv.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.polazniciDgv.MultiSelect = false;
            this.polazniciDgv.Name = "polazniciDgv";
            this.polazniciDgv.ReadOnly = true;
            this.polazniciDgv.RowTemplate.Height = 24;
            this.polazniciDgv.Size = new System.Drawing.Size(817, 1126);
            this.polazniciDgv.TabIndex = 0;
            this.polazniciDgv.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.polazniciDgv_CellEndEdit);
            this.polazniciDgv.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.polazniciDgv_DataError);
            // 
            // pretragaTB
            // 
            this.pretragaTB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pretragaTB.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pretragaTB.Location = new System.Drawing.Point(348, 35);
            this.pretragaTB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.pretragaTB.Name = "pretragaTB";
            this.pretragaTB.Size = new System.Drawing.Size(311, 28);
            this.pretragaTB.TabIndex = 1;
            this.pretragaTB.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.datotekaToolStripMenuItem,
            this.upravljanjeToolStripMenuItem,
            this.pomoćToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(5, 1, 0, 1);
            this.menuStrip1.Size = new System.Drawing.Size(971, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // datotekaToolStripMenuItem
            // 
            this.datotekaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.zatvoriToolStripMenuItem});
            this.datotekaToolStripMenuItem.Name = "datotekaToolStripMenuItem";
            this.datotekaToolStripMenuItem.Size = new System.Drawing.Size(66, 22);
            this.datotekaToolStripMenuItem.Text = "Datoteka";
            // 
            // zatvoriToolStripMenuItem
            // 
            this.zatvoriToolStripMenuItem.Name = "zatvoriToolStripMenuItem";
            this.zatvoriToolStripMenuItem.Size = new System.Drawing.Size(111, 22);
            this.zatvoriToolStripMenuItem.Text = "Zatvori";
            this.zatvoriToolStripMenuItem.Click += new System.EventHandler(this.zatvoriToolStripMenuItem_Click);
            // 
            // upravljanjeToolStripMenuItem
            // 
            this.upravljanjeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.uveziKontakteToolStripMenuItem,
            this.izveziPodatkeToolStripMenuItem,
            this.izbrišiSvePodatkeToolStripMenuItem});
            this.upravljanjeToolStripMenuItem.Name = "upravljanjeToolStripMenuItem";
            this.upravljanjeToolStripMenuItem.Size = new System.Drawing.Size(78, 22);
            this.upravljanjeToolStripMenuItem.Text = "Upravljanje";
            // 
            // uveziKontakteToolStripMenuItem
            // 
            this.uveziKontakteToolStripMenuItem.Name = "uveziKontakteToolStripMenuItem";
            this.uveziKontakteToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.uveziKontakteToolStripMenuItem.Text = "Uvezi podatke";
            this.uveziKontakteToolStripMenuItem.Click += new System.EventHandler(this.uveziKontakteToolStripMenuItem_Click);
            // 
            // izveziPodatkeToolStripMenuItem
            // 
            this.izveziPodatkeToolStripMenuItem.Name = "izveziPodatkeToolStripMenuItem";
            this.izveziPodatkeToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.izveziPodatkeToolStripMenuItem.Text = "Izvezi podatke";
            this.izveziPodatkeToolStripMenuItem.Click += new System.EventHandler(this.izveziPodatkeToolStripMenuItem_Click);
            // 
            // izbrišiSvePodatkeToolStripMenuItem
            // 
            this.izbrišiSvePodatkeToolStripMenuItem.Name = "izbrišiSvePodatkeToolStripMenuItem";
            this.izbrišiSvePodatkeToolStripMenuItem.Size = new System.Drawing.Size(170, 22);
            this.izbrišiSvePodatkeToolStripMenuItem.Text = "Izbriši sve podatke";
            this.izbrišiSvePodatkeToolStripMenuItem.Click += new System.EventHandler(this.izbrišiSvePodatkeToolStripMenuItem_Click);
            // 
            // pomoćToolStripMenuItem
            // 
            this.pomoćToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.oProgramuToolStripMenuItem});
            this.pomoćToolStripMenuItem.Name = "pomoćToolStripMenuItem";
            this.pomoćToolStripMenuItem.Size = new System.Drawing.Size(57, 22);
            this.pomoćToolStripMenuItem.Text = "Pomoć";
            // 
            // oProgramuToolStripMenuItem
            // 
            this.oProgramuToolStripMenuItem.Name = "oProgramuToolStripMenuItem";
            this.oProgramuToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.oProgramuToolStripMenuItem.Text = "O programu";
            this.oProgramuToolStripMenuItem.Click += new System.EventHandler(this.oProgramuToolStripMenuItem_Click);
            // 
            // pretragaLbl
            // 
            this.pretragaLbl.AutoSize = true;
            this.pretragaLbl.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pretragaLbl.Location = new System.Drawing.Point(265, 38);
            this.pretragaLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.pretragaLbl.Name = "pretragaLbl";
            this.pretragaLbl.Size = new System.Drawing.Size(80, 24);
            this.pretragaLbl.TabIndex = 3;
            this.pretragaLbl.Text = "Pretraga";
            // 
            // vrticLbl
            // 
            this.vrticLbl.AutoSize = true;
            this.vrticLbl.Location = new System.Drawing.Point(4, 183);
            this.vrticLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.vrticLbl.Name = "vrticLbl";
            this.vrticLbl.Size = new System.Drawing.Size(28, 13);
            this.vrticLbl.TabIndex = 30;
            this.vrticLbl.Text = "Vrtić";
            // 
            // skupinaLbl
            // 
            this.skupinaLbl.AutoSize = true;
            this.skupinaLbl.Location = new System.Drawing.Point(3, 254);
            this.skupinaLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.skupinaLbl.Name = "skupinaLbl";
            this.skupinaLbl.Size = new System.Drawing.Size(46, 13);
            this.skupinaLbl.TabIndex = 29;
            this.skupinaLbl.Text = "Skupina";
            // 
            // specificnostLbl
            // 
            this.specificnostLbl.AutoSize = true;
            this.specificnostLbl.Location = new System.Drawing.Point(5, 487);
            this.specificnostLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.specificnostLbl.Name = "specificnostLbl";
            this.specificnostLbl.Size = new System.Drawing.Size(65, 13);
            this.specificnostLbl.TabIndex = 28;
            this.specificnostLbl.Text = "Specifičnost";
            // 
            // programLbl
            // 
            this.programLbl.AutoSize = true;
            this.programLbl.Location = new System.Drawing.Point(4, 312);
            this.programLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.programLbl.Name = "programLbl";
            this.programLbl.Size = new System.Drawing.Size(46, 13);
            this.programLbl.TabIndex = 27;
            this.programLbl.Text = "Program";
            // 
            // vrticCLB
            // 
            this.vrticCLB.CheckOnClick = true;
            this.vrticCLB.FormattingEnabled = true;
            this.vrticCLB.Location = new System.Drawing.Point(3, 197);
            this.vrticCLB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.vrticCLB.Name = "vrticCLB";
            this.vrticCLB.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.vrticCLB.Size = new System.Drawing.Size(101, 49);
            this.vrticCLB.TabIndex = 24;
            this.vrticCLB.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.vrticCLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // specificnostCLB
            // 
            this.specificnostCLB.CheckOnClick = true;
            this.specificnostCLB.FormattingEnabled = true;
            this.specificnostCLB.Location = new System.Drawing.Point(5, 501);
            this.specificnostCLB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.specificnostCLB.Name = "specificnostCLB";
            this.specificnostCLB.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.specificnostCLB.Size = new System.Drawing.Size(101, 79);
            this.specificnostCLB.TabIndex = 21;
            this.specificnostCLB.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.specificnostCLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // programCLB
            // 
            this.programCLB.CheckOnClick = true;
            this.programCLB.FormattingEnabled = true;
            this.programCLB.Location = new System.Drawing.Point(4, 328);
            this.programCLB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.programCLB.Name = "programCLB";
            this.programCLB.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.programCLB.Size = new System.Drawing.Size(101, 79);
            this.programCLB.TabIndex = 20;
            this.programCLB.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.programCLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // skupinaCLB
            // 
            this.skupinaCLB.CheckOnClick = true;
            this.skupinaCLB.FormattingEnabled = true;
            this.skupinaCLB.Location = new System.Drawing.Point(3, 268);
            this.skupinaCLB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.skupinaCLB.Name = "skupinaCLB";
            this.skupinaCLB.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.skupinaCLB.Size = new System.Drawing.Size(101, 34);
            this.skupinaCLB.TabIndex = 19;
            this.skupinaCLB.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.skupinaCLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // resetFilterBtn
            // 
            this.resetFilterBtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.resetFilterBtn.Location = new System.Drawing.Point(662, 35);
            this.resetFilterBtn.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.resetFilterBtn.Name = "resetFilterBtn";
            this.resetFilterBtn.Size = new System.Drawing.Size(68, 26);
            this.resetFilterBtn.TabIndex = 35;
            this.resetFilterBtn.Text = "Reset filter";
            this.resetFilterBtn.UseVisualStyleBackColor = true;
            this.resetFilterBtn.Click += new System.EventHandler(this.resetFilterBtn_Click);
            // 
            // pronadenoLbl
            // 
            this.pronadenoLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pronadenoLbl.AutoSize = true;
            this.pronadenoLbl.Location = new System.Drawing.Point(819, 42);
            this.pronadenoLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.pronadenoLbl.Name = "pronadenoLbl";
            this.pronadenoLbl.Size = new System.Drawing.Size(63, 13);
            this.pronadenoLbl.TabIndex = 36;
            this.pronadenoLbl.Text = "Pronađeno:";
            // 
            // brojRezultataLbl
            // 
            this.brojRezultataLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.brojRezultataLbl.AutoSize = true;
            this.brojRezultataLbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.brojRezultataLbl.Location = new System.Drawing.Point(885, 42);
            this.brojRezultataLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.brojRezultataLbl.Name = "brojRezultataLbl";
            this.brojRezultataLbl.Size = new System.Drawing.Size(15, 15);
            this.brojRezultataLbl.TabIndex = 37;
            this.brojRezultataLbl.Text = "0";
            // 
            // upisLbl
            // 
            this.upisLbl.AutoSize = true;
            this.upisLbl.Location = new System.Drawing.Point(2, 727);
            this.upisLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.upisLbl.Name = "upisLbl";
            this.upisLbl.Size = new System.Drawing.Size(28, 13);
            this.upisLbl.TabIndex = 43;
            this.upisLbl.Text = "Upis";
            // 
            // spolLbl
            // 
            this.spolLbl.AutoSize = true;
            this.spolLbl.Location = new System.Drawing.Point(3, 123);
            this.spolLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.spolLbl.Name = "spolLbl";
            this.spolLbl.Size = new System.Drawing.Size(28, 13);
            this.spolLbl.TabIndex = 42;
            this.spolLbl.Text = "Spol";
            // 
            // godineLbl
            // 
            this.godineLbl.AutoSize = true;
            this.godineLbl.Location = new System.Drawing.Point(2, 0);
            this.godineLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.godineLbl.Name = "godineLbl";
            this.godineLbl.Size = new System.Drawing.Size(27, 13);
            this.godineLbl.TabIndex = 41;
            this.godineLbl.Text = "Dob";
            // 
            // upisCLB
            // 
            this.upisCLB.CheckOnClick = true;
            this.upisCLB.FormattingEnabled = true;
            this.upisCLB.Location = new System.Drawing.Point(2, 738);
            this.upisCLB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.upisCLB.Name = "upisCLB";
            this.upisCLB.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.upisCLB.Size = new System.Drawing.Size(101, 49);
            this.upisCLB.TabIndex = 40;
            this.upisCLB.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.upisCLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // spolCLB
            // 
            this.spolCLB.CheckOnClick = true;
            this.spolCLB.FormattingEnabled = true;
            this.spolCLB.Location = new System.Drawing.Point(2, 139);
            this.spolCLB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.spolCLB.Name = "spolCLB";
            this.spolCLB.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.spolCLB.Size = new System.Drawing.Size(103, 34);
            this.spolCLB.TabIndex = 39;
            this.spolCLB.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.spolCLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // godineCLB
            // 
            this.godineCLB.CheckOnClick = true;
            this.godineCLB.FormattingEnabled = true;
            this.godineCLB.Location = new System.Drawing.Point(2, 12);
            this.godineCLB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.godineCLB.Name = "godineCLB";
            this.godineCLB.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.godineCLB.Size = new System.Drawing.Size(119, 109);
            this.godineCLB.TabIndex = 38;
            this.godineCLB.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.godineCLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // dokumentacijaLbl
            // 
            this.dokumentacijaLbl.AutoSize = true;
            this.dokumentacijaLbl.Location = new System.Drawing.Point(5, 587);
            this.dokumentacijaLbl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dokumentacijaLbl.Name = "dokumentacijaLbl";
            this.dokumentacijaLbl.Size = new System.Drawing.Size(78, 13);
            this.dokumentacijaLbl.TabIndex = 44;
            this.dokumentacijaLbl.Text = "Dokumentacija";
            // 
            // dokumentacijaCLB
            // 
            this.dokumentacijaCLB.CheckOnClick = true;
            this.dokumentacijaCLB.FormattingEnabled = true;
            this.dokumentacijaCLB.Location = new System.Drawing.Point(5, 603);
            this.dokumentacijaCLB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.dokumentacijaCLB.Name = "dokumentacijaCLB";
            this.dokumentacijaCLB.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.dokumentacijaCLB.Size = new System.Drawing.Size(101, 49);
            this.dokumentacijaCLB.TabIndex = 45;
            this.dokumentacijaCLB.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.dokumentacijaCLB.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.lbSuradnici);
            this.panel1.Controls.Add(this.lblSuradnici);
            this.panel1.Controls.Add(this.lbObveznik);
            this.panel1.Controls.Add(this.lblObveznk);
            this.panel1.Controls.Add(this.lbDokVrtic);
            this.panel1.Controls.Add(this.lblDokVrtic);
            this.panel1.Controls.Add(this.godineCLB);
            this.panel1.Controls.Add(this.specificnostCLB);
            this.panel1.Controls.Add(this.dokumentacijaCLB);
            this.panel1.Controls.Add(this.specificnostLbl);
            this.panel1.Controls.Add(this.godineLbl);
            this.panel1.Controls.Add(this.dokumentacijaLbl);
            this.panel1.Controls.Add(this.spolLbl);
            this.panel1.Controls.Add(this.spolCLB);
            this.panel1.Controls.Add(this.vrticCLB);
            this.panel1.Controls.Add(this.programCLB);
            this.panel1.Controls.Add(this.skupinaCLB);
            this.panel1.Controls.Add(this.programLbl);
            this.panel1.Controls.Add(this.upisCLB);
            this.panel1.Controls.Add(this.upisLbl);
            this.panel1.Controls.Add(this.skupinaLbl);
            this.panel1.Controls.Add(this.vrticLbl);
            this.panel1.Location = new System.Drawing.Point(0, 20);
            this.panel1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(133, 957);
            this.panel1.TabIndex = 46;
            // 
            // lbSuradnici
            // 
            this.lbSuradnici.CheckOnClick = true;
            this.lbSuradnici.FormattingEnabled = true;
            this.lbSuradnici.Location = new System.Drawing.Point(0, 805);
            this.lbSuradnici.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lbSuradnici.Name = "lbSuradnici";
            this.lbSuradnici.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbSuradnici.Size = new System.Drawing.Size(101, 94);
            this.lbSuradnici.TabIndex = 53;
            this.lbSuradnici.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.lbSuradnici.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // lblSuradnici
            // 
            this.lblSuradnici.AutoSize = true;
            this.lblSuradnici.Location = new System.Drawing.Point(2, 790);
            this.lblSuradnici.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblSuradnici.Name = "lblSuradnici";
            this.lblSuradnici.Size = new System.Drawing.Size(85, 13);
            this.lblSuradnici.TabIndex = 52;
            this.lblSuradnici.Text = "Stručni suradnici";
            // 
            // lbObveznik
            // 
            this.lbObveznik.CheckOnClick = true;
            this.lbObveznik.FormattingEnabled = true;
            this.lbObveznik.Location = new System.Drawing.Point(5, 428);
            this.lbObveznik.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lbObveznik.Name = "lbObveznik";
            this.lbObveznik.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbObveznik.Size = new System.Drawing.Size(101, 49);
            this.lbObveznik.TabIndex = 50;
            this.lbObveznik.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.lbObveznik.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // lblObveznk
            // 
            this.lblObveznk.AutoSize = true;
            this.lblObveznk.Location = new System.Drawing.Point(2, 413);
            this.lblObveznk.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblObveznk.Name = "lblObveznk";
            this.lblObveznk.Size = new System.Drawing.Size(87, 13);
            this.lblObveznk.TabIndex = 51;
            this.lblObveznk.Text = "Školski obveznik";
            // 
            // lbDokVrtic
            // 
            this.lbDokVrtic.CheckOnClick = true;
            this.lbDokVrtic.FormattingEnabled = true;
            this.lbDokVrtic.Location = new System.Drawing.Point(2, 675);
            this.lbDokVrtic.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.lbDokVrtic.Name = "lbDokVrtic";
            this.lbDokVrtic.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.lbDokVrtic.Size = new System.Drawing.Size(101, 49);
            this.lbDokVrtic.TabIndex = 46;
            this.lbDokVrtic.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.ItemCheckedSearch);
            this.lbDokVrtic.MouseDown += new System.Windows.Forms.MouseEventHandler(this.godineCLB_MouseDown);
            // 
            // lblDokVrtic
            // 
            this.lblDokVrtic.AutoSize = true;
            this.lblDokVrtic.Location = new System.Drawing.Point(2, 660);
            this.lblDokVrtic.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblDokVrtic.Name = "lblDokVrtic";
            this.lblDokVrtic.Size = new System.Drawing.Size(107, 13);
            this.lblDokVrtic.TabIndex = 47;
            this.lblDokVrtic.Text = "Dokumentacija - vrtić";
            // 
            // PretragaDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(971, 979);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.brojRezultataLbl);
            this.Controls.Add(this.pronadenoLbl);
            this.Controls.Add(this.resetFilterBtn);
            this.Controls.Add(this.pretragaLbl);
            this.Controls.Add(this.pretragaTB);
            this.Controls.Add(this.polazniciDgv);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "PretragaDialog";
            this.Text = "Pretraga polaznika";
            ((System.ComponentModel.ISupportInitialize)(this.polazniciDgv)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView polazniciDgv;
        private System.Windows.Forms.TextBox pretragaTB;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem datotekaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem zatvoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem upravljanjeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uveziKontakteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izveziPodatkeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem pomoćToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem oProgramuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem izbrišiSvePodatkeToolStripMenuItem;
        private System.Windows.Forms.Label pretragaLbl;
        private System.Windows.Forms.Label vrticLbl;
        private System.Windows.Forms.Label skupinaLbl;
        private System.Windows.Forms.Label specificnostLbl;
        private System.Windows.Forms.Label programLbl;
        private System.Windows.Forms.CheckedListBox vrticCLB;
        private System.Windows.Forms.CheckedListBox specificnostCLB;
        private System.Windows.Forms.CheckedListBox programCLB;
        private System.Windows.Forms.CheckedListBox skupinaCLB;
        private System.Windows.Forms.Button resetFilterBtn;
        private System.Windows.Forms.Label pronadenoLbl;
        private System.Windows.Forms.Label brojRezultataLbl;
        private System.Windows.Forms.Label upisLbl;
        private System.Windows.Forms.Label spolLbl;
        private System.Windows.Forms.Label godineLbl;
        private System.Windows.Forms.CheckedListBox upisCLB;
        private System.Windows.Forms.CheckedListBox spolCLB;
        private System.Windows.Forms.CheckedListBox godineCLB;
        private System.Windows.Forms.Label dokumentacijaLbl;
        private System.Windows.Forms.CheckedListBox dokumentacijaCLB;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.CheckedListBox lbObveznik;
        private System.Windows.Forms.Label lblObveznk;
        private System.Windows.Forms.CheckedListBox lbDokVrtic;
        private System.Windows.Forms.Label lblDokVrtic;
        private System.Windows.Forms.CheckedListBox lbSuradnici;
        private System.Windows.Forms.Label lblSuradnici;
    }
}

