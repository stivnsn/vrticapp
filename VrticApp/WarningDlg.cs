﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VrticApp2
{
    public partial class WarningDlg : Form
    {
        public WarningDlg(string txt)
        {
            InitializeComponent();
            txtLbl.Text = txt;
        }

        private void ureduBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
