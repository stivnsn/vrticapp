﻿namespace VrticApp2
{
    partial class OProgramu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OProgramu));
            this.ureduBtn = new System.Windows.Forms.Button();
            this.txtTxt = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // ureduBtn
            // 
            this.ureduBtn.Location = new System.Drawing.Point(106, 208);
            this.ureduBtn.Name = "ureduBtn";
            this.ureduBtn.Size = new System.Drawing.Size(75, 23);
            this.ureduBtn.TabIndex = 0;
            this.ureduBtn.Text = "U redu";
            this.ureduBtn.UseVisualStyleBackColor = true;
            this.ureduBtn.Click += new System.EventHandler(this.ureduBtn_Click);
            // 
            // txtTxt
            // 
            this.txtTxt.BackColor = System.Drawing.SystemColors.Control;
            this.txtTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtTxt.Location = new System.Drawing.Point(42, 48);
            this.txtTxt.Name = "txtTxt";
            this.txtTxt.ReadOnly = true;
            this.txtTxt.Size = new System.Drawing.Size(250, 126);
            this.txtTxt.TabIndex = 1;
            this.txtTxt.Text = resources.GetString("txtTxt.Text");
            // 
            // OProgramu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 292);
            this.Controls.Add(this.txtTxt);
            this.Controls.Add(this.ureduBtn);
            this.Name = "OProgramu";
            this.Text = "O programu";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ureduBtn;
        private System.Windows.Forms.RichTextBox txtTxt;
    }
}