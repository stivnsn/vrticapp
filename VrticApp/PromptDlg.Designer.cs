﻿namespace VrticApp2
{
    partial class PromptDlg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.daBtn = new System.Windows.Forms.Button();
            this.odustaniBtn = new System.Windows.Forms.Button();
            this.txtLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // daBtn
            // 
            this.daBtn.Location = new System.Drawing.Point(187, 209);
            this.daBtn.Name = "daBtn";
            this.daBtn.Size = new System.Drawing.Size(103, 45);
            this.daBtn.TabIndex = 0;
            this.daBtn.Text = "Da!";
            this.daBtn.UseVisualStyleBackColor = true;
            this.daBtn.Click += new System.EventHandler(this.izbrisiBtn_Click);
            // 
            // odustaniBtn
            // 
            this.odustaniBtn.Location = new System.Drawing.Point(296, 220);
            this.odustaniBtn.Name = "odustaniBtn";
            this.odustaniBtn.Size = new System.Drawing.Size(75, 23);
            this.odustaniBtn.TabIndex = 1;
            this.odustaniBtn.Text = "Odustani";
            this.odustaniBtn.UseVisualStyleBackColor = true;
            this.odustaniBtn.Click += new System.EventHandler(this.odustaniBtn_Click);
            // 
            // txtLbl
            // 
            this.txtLbl.AutoSize = true;
            this.txtLbl.Location = new System.Drawing.Point(12, 83);
            this.txtLbl.Name = "txtLbl";
            this.txtLbl.Size = new System.Drawing.Size(140, 17);
            this.txtLbl.TabIndex = 2;
            this.txtLbl.Text = "Tekst nije postavljen!";
            // 
            // Dialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(382, 259);
            this.Controls.Add(this.txtLbl);
            this.Controls.Add(this.odustaniBtn);
            this.Controls.Add(this.daBtn);
            this.Name = "Dialog";
            this.Text = "Dialog";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button daBtn;
        private System.Windows.Forms.Button odustaniBtn;
        private System.Windows.Forms.Label txtLbl;
    }
}