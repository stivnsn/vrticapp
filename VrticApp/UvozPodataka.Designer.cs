﻿namespace VrticApp2
{
    partial class UvozPodataka
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ucitaniDGV = new System.Windows.Forms.DataGridView();
            this.ucitajBtn = new System.Windows.Forms.Button();
            this.uveziBtn = new System.Windows.Forms.Button();
            this.odustaniBtn = new System.Windows.Forms.Button();
            this.progressControl1 = new VrticApp2.progressControl();
            ((System.ComponentModel.ISupportInitialize)(this.ucitaniDGV)).BeginInit();
            this.SuspendLayout();
            // 
            // ucitaniDGV
            // 
            this.ucitaniDGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.ucitaniDGV.Location = new System.Drawing.Point(12, 91);
            this.ucitaniDGV.MultiSelect = false;
            this.ucitaniDGV.Name = "ucitaniDGV";
            this.ucitaniDGV.ReadOnly = true;
            this.ucitaniDGV.RowTemplate.Height = 24;
            this.ucitaniDGV.Size = new System.Drawing.Size(1145, 541);
            this.ucitaniDGV.TabIndex = 0;
            // 
            // ucitajBtn
            // 
            this.ucitajBtn.Location = new System.Drawing.Point(12, 22);
            this.ucitajBtn.Name = "ucitajBtn";
            this.ucitajBtn.Size = new System.Drawing.Size(125, 41);
            this.ucitajBtn.TabIndex = 1;
            this.ucitajBtn.Text = "Učitaj datoteku";
            this.ucitajBtn.UseVisualStyleBackColor = true;
            this.ucitajBtn.Click += new System.EventHandler(this.UcitajBtn_Click);
            // 
            // uveziBtn
            // 
            this.uveziBtn.Enabled = false;
            this.uveziBtn.Location = new System.Drawing.Point(143, 22);
            this.uveziBtn.Name = "uveziBtn";
            this.uveziBtn.Size = new System.Drawing.Size(102, 41);
            this.uveziBtn.TabIndex = 2;
            this.uveziBtn.Text = "Uvezi";
            this.uveziBtn.UseVisualStyleBackColor = true;
            this.uveziBtn.Click += new System.EventHandler(this.uveziBtn_Click);
            // 
            // odustaniBtn
            // 
            this.odustaniBtn.Location = new System.Drawing.Point(251, 22);
            this.odustaniBtn.Name = "odustaniBtn";
            this.odustaniBtn.Size = new System.Drawing.Size(97, 41);
            this.odustaniBtn.TabIndex = 3;
            this.odustaniBtn.Text = "Odustani";
            this.odustaniBtn.UseVisualStyleBackColor = true;
            this.odustaniBtn.Click += new System.EventHandler(this.odustaniBtn_Click);
            // 
            // progressControl1
            // 
            this.progressControl1.Location = new System.Drawing.Point(887, 34);
            this.progressControl1.Name = "progressControl1";
            this.progressControl1.Size = new System.Drawing.Size(236, 29);
            this.progressControl1.TabIndex = 4;
            this.progressControl1.Visible = false;
            // 
            // UvozPodataka
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1169, 644);
            this.Controls.Add(this.progressControl1);
            this.Controls.Add(this.odustaniBtn);
            this.Controls.Add(this.uveziBtn);
            this.Controls.Add(this.ucitajBtn);
            this.Controls.Add(this.ucitaniDGV);
            this.Name = "UvozPodataka";
            this.Text = "UvozPodataka";
            ((System.ComponentModel.ISupportInitialize)(this.ucitaniDGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView ucitaniDGV;
        private System.Windows.Forms.Button ucitajBtn;
        private System.Windows.Forms.Button uveziBtn;
        private System.Windows.Forms.Button odustaniBtn;
        private progressControl progressControl1;
    }
}