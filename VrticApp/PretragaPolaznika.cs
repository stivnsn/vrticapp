﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VrticApp2
{
    public partial class PretragaDialog : Form
    {
        DataTable _polazniciDT = new DataTable();
        List<Polaznik> _polaznici = new List<Polaznik>();
        VrticDBManager _dbManager;
        public PretragaDialog()
        {   
            InitializeComponent();
            _dbManager = new VrticDBManager();
            refreshDGVFromDb();
            setCLBDefault(Polaznik.upisi, ref upisCLB);
            setCLBDefault(Polaznik.spolovi, ref spolCLB);
            setCLBDefault(Polaznik.specificnosti, ref specificnostCLB);
            setCLBDefault(Polaznik.objekti, ref vrticCLB);
            setCLBDefault(Polaznik.programi, ref programCLB);
            setCLBDefault(Polaznik.suradnici, ref lbSuradnici);
            setCLBDefault(Polaznik.školskiObveznik, ref lbObveznik);
            setCLBDefault(Polaznik.dokumentacijeUVrticu, ref lbDokVrtic);
            setCLBDefault(Polaznik.skupine, ref skupinaCLB);
            setCLBDefault(Polaznik.dokumentacije, ref dokumentacijaCLB);
            setCLBDefault(Polaznik.dobi, ref godineCLB);
        }

        private void setCLBDefault(string [] items, ref CheckedListBox clb)
        {
            foreach (string item in items)
            {
                clb.Items.Add(item);
            }
        }

        private void zatvoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void uveziKontakteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UvozPodataka uvozDlg = new UvozPodataka(_dbManager);
            uvozDlg.ShowDialog();
            refreshDGVFromDb();
        }

        private void izveziPodatkeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveDlg = new SaveFileDialog();
            saveDlg.Filter = "CSV files (*.csv)|*.csv";
            saveDlg.ShowDialog();
            string csvFilename = saveDlg.FileName;
            
            CSVManager csvManager = new CSVManager(csvFilename, ref _polaznici);
            csvManager.writeCSV();
        }

        private void oProgramuToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OProgramu oProgramu = new OProgramu();
            oProgramu.ShowDialog();
        }

        static Timer timer = new Timer();
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            timedSearch(700);
        }

        private void TimerEventProcessor(Object myObject,
                                            EventArgs myEventArgs)
        {
            timer.Stop();
            searchPolaznici();
        }
        private void timedSearch(int count)
        {
            if (timer.Enabled)
            {
                timer.Tick -= new EventHandler(TimerEventProcessor);
                timer.Stop();
            }
            timer.Tick += new EventHandler(TimerEventProcessor);
            timer.Interval = count;
            timer.Start();
        }

        private bool isMeetingCLBFilter(Polaznik element)
        {
            bool result = (upisCLB.CheckedItems.Count == 0 || upisCLB.CheckedItems.Contains(element.Upis))
            && (vrticCLB.CheckedItems.Count == 0 || vrticCLB.CheckedItems.Contains(element.Objekt))
            && (skupinaCLB.CheckedItems.Count == 0 || skupinaCLB.CheckedItems.Contains(element.Skupina))
            && (programCLB.CheckedItems.Count == 0 || programCLB.CheckedItems.Contains(element.Program))
            && (lbDokVrtic.CheckedItems.Count == 0 || lbDokVrtic.CheckedItems.Contains(element.DokumentacijaUVrticu))
            && (lbObveznik.CheckedItems.Count == 0 || lbObveznik.CheckedItems.Contains(element.ŠkolskiObveznici))
            && ((lbSuradnici.CheckedItems.Count == 0) || containsStrucniSuradnici(element.StručniSuradnici.Split(','), lbSuradnici.CheckedItems))
            && (specificnostCLB.CheckedItems.Count == 0 || specificnostCLB.CheckedItems.Contains(element.Specifičnost))
            && (godineCLB.CheckedItems.Count == 0 || godineCLB.CheckedItems.Contains(element.Dob))
            && (dokumentacijaCLB.CheckedItems.Count == 0 || dokumentacijaCLB.CheckedItems.Contains(element.Dokumentacija))
            && (spolCLB.CheckedItems.Count == 0 || spolCLB.CheckedItems.Contains(element.Spol));
            return result;
        }

        private bool containsStrucniSuradnici(string[] suradnici, CheckedListBox.CheckedItemCollection itemCollection)
        {
            if (suradnici.Length == 0) return true;
            foreach (string suradnik in suradnici)
            {
                if (itemCollection.Contains(suradnik.Trim()))
                    return true;
            }
            return false;
        }

        private void searchPolaznici()
        {
            //(dataGridViewFields.DataSource as DataTable).DefaultView.RowFilter = string.Format("Field = '{0}'", textBoxFilter.Text);
            //http://www.csharp-examples.net/dataview-rowfilter/
            List<Polaznik> polazniciFilter = new List<Polaznik>();
            string searchFilter = pretragaTB.Text;
            if (searchFilter.StartsWith("\"") && searchFilter.EndsWith("\""))
            {
                foreach (Polaznik element in _polaznici)
                {
                    searchFilter = searchFilter.Trim('"');
                    if ((element.Ime.ToLower().Equals(searchFilter.ToLower())
                        || element.Prezime.ToLower().Equals(searchFilter.ToLower()))
                        && isMeetingCLBFilter(element))
                    {
                        polazniciFilter.Add(element);
                    }
                }
            }
            else
            {
                foreach (Polaznik element in _polaznici)
                {
                    if ((element.Ime.ToLower().Contains(searchFilter.ToLower())
                        || element.Prezime.ToLower().Contains(searchFilter.ToLower()))
                        && isMeetingCLBFilter(element))
                    {
                        polazniciFilter.Add(element);
                    }
                }
            }
            reloadDgv(ref polazniciFilter);
        }
        
        void reloadDgv(ref List<Polaznik> polazniciList)
        {
            polazniciDgv.DataSource = null;
            polazniciDgv.DataSource = polazniciList;
            brojRezultataLbl.Text = polazniciList.Count.ToString();
        }

        private void refreshDGVFromDb()
        {
            _dbManager.GetPolaznici(ref _polaznici);
            reloadDgv(ref _polaznici);
        }

        private void izbrišiSvePodatkeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            PromptDlg dialog = new PromptDlg(ref _dbManager, "Klikom na OK! brišu se svi polaznici.");
            dialog.ShowDialog();
            refreshDGVFromDb();
        }


        private void polazniciDgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //Polaznik polaznik = (Polaznik)((DataGridView)sender).Rows[e.RowIndex].DataBoundItem;
            //List<Polaznik> polaznici = new List<Polaznik>();
            //polaznici.Add(polaznik);
            //_dbManager.SetPolaznici(polaznici);
        }

        private void polazniciDgv_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            WarningDlg dlg = new WarningDlg("Podatak nije u dobrom formatu. Pokušajte ponovno ili Esc!");
            dlg.ShowDialog();
        }

        private void ItemCheckedSearch(object sender, ItemCheckEventArgs e)
        {
            CheckedListBox clb = (CheckedListBox)sender;
            clb.ItemCheck -= ItemCheckedSearch;
            clb.SetItemCheckState(e.Index, e.NewValue);
            clb.ItemCheck += ItemCheckedSearch;
            searchPolaznici();
        }
        private void uncheckCBL(ref CheckedListBox obj)
        {
            foreach (int item in obj.CheckedIndices)
            {
                obj.SetItemCheckState(item, CheckState.Unchecked);
            }
        }

        private void resetFilterBtn_Click(object sender, EventArgs e)
        {
            uncheckCBL(ref upisCLB);
            uncheckCBL(ref godineCLB);
            uncheckCBL(ref spolCLB);
            uncheckCBL(ref vrticCLB);
            uncheckCBL(ref lbSuradnici);
            uncheckCBL(ref lbDokVrtic);
            uncheckCBL(ref lbObveznik);
            uncheckCBL(ref specificnostCLB);
            uncheckCBL(ref skupinaCLB);
            uncheckCBL(ref dokumentacijaCLB);
            uncheckCBL(ref programCLB);
        }

        private void godineCLB_MouseDown(object sender, MouseEventArgs e)
        {
            CheckedListBox clb = (CheckedListBox)sender;
            int index = clb.IndexFromPoint(e.Location);
            clb.SetItemChecked(index, !clb.GetItemChecked(index));
        }
        
    }
}
