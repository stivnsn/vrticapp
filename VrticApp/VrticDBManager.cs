﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.Data;
using System.IO;

namespace VrticApp2
{
    public class VrticDBManager
    {
        public string logError;
        SQLiteConnection _SQLiteConnection;
        private System.Data.ConnectionState state;
        string dbName = "Polaznici.sqlite";
        DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        public VrticDBManager()
        {
            CreateNewDbTry();
        }

        bool ConnectToDb()
        {
            _SQLiteConnection = new SQLiteConnection("DataSource=" + dbName + ";Version=3;");
            _SQLiteConnection.Open();
            if (_SQLiteConnection.State != System.Data.ConnectionState.Open)
            {
                logError += "Connection is not open. Try to create database. \n";
                return false;
            }
            return true;
        }

        public ConnectionState State { get => _SQLiteConnection.State; }

        TimeSpan ConvertDateToTimestamp(DateTime datum)
        {

            return TimeSpan.FromSeconds(datum.Subtract(epoch).TotalSeconds);
        }

        DateTime ConvertTimeStampToDate(TimeSpan timestamp)
        {
            return epoch.Add(timestamp);
        }

        public bool CreateNewDbTry()
        {
            try
            {
                if (File.Exists(dbName))
                {
                    return false;
                }
                SQLiteConnection.CreateFile(dbName);
                if (!ConnectToDb() && state != ConnectionState.Open)
                {
                    return false;
                }
                SQLiteCommand createTblCmd = new SQLiteCommand("CREATE TABLE polaznici (ime NVARCHAR(20), prezime NVARCHAR(30), datum_rod DATE, "
                    + "vrtic NVARCHAR(30), spol NVARCHAR(20), skupina NVARCHAR(20), program NVARCHAR(20), skolskiObveznik NVARCHAR(20), specificnost NVARCHAR(20), upis NVARCHAR(20), dokumentacija NVARCHAR(20), "
                    + " dokumentacijaUVrticu NVARCHAR(20), strucniSuradnici NVARCHAR(20), UNIQUE(ime,prezime,datum_rod) ON CONFLICT REPLACE);", _SQLiteConnection);
                createTblCmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                logError += ex.Message + "\n";
            }
            return false;
        }
        
        public bool SetPolaznici(List<Polaznik> polaznici, Action<int> progressFunc)
        {
            try
            {
                if (state != ConnectionState.Open)
                {
                    if (!ConnectToDb() && state != ConnectionState.Open)
                    {
                        return false;
                    }
                }
                double division = (100.0 / polaznici.Count);
                int tick = (int)Math.Floor(division);
                foreach (Polaznik polaznik in polaznici)
                {
                    string polaznikVals = "'" + polaznik.Ime + "', '" + polaznik.Prezime + "', '" + polaznik.DatumRođenja.ToString("yyyy-MM-dd") + "', '" + polaznik.Spol
                        + "', '" + polaznik.Objekt + "', '" + polaznik.Skupina + "', '" + polaznik.Program + "', '" + polaznik.ŠkolskiObveznici + "', '" + polaznik.Specifičnost + "', '" 
                        + polaznik.Dokumentacija + "', '" + polaznik.DokumentacijaUVrticu + "', '" + polaznik.Upis + "', '" + polaznik.StručniSuradnici + "'";
                    SQLiteCommand createTblCmd = new SQLiteCommand("INSERT INTO polaznici(ime, prezime, datum_rod, spol, vrtic, skupina, "
                        + "program, skolskiObveznik, specificnost, dokumentacija, dokumentacijaUVrticu, upis, strucniSuradnici) VALUES (" + polaznikVals + ");", _SQLiteConnection);
                    createTblCmd.ExecuteNonQuery();
                    progressFunc(tick);
                }
                return true;
            }
            catch (Exception ex)
            {
                logError += ex.Message + "\n";
            }
            return false;
        }

        public bool GetPolaznici(ref List<Polaznik> polaznici)
        {
            try
            {
                if (state != ConnectionState.Open)
                {
                    if (!ConnectToDb() && state != ConnectionState.Open)
                    {
                        return false;
                    }
                }
                SQLiteCommand createTblCmd = new SQLiteCommand("SELECT * FROM polaznici;", _SQLiteConnection);
                SQLiteDataReader reader = createTblCmd.ExecuteReader();
                polaznici.Clear();
                DataTable dt = new DataTable();
                while (reader.Read())
                {
                    Polaznik polaznik = new Polaznik
                    {
                        Ime = (string)reader["ime"],
                        Prezime = (string)reader["prezime"],
                        DatumRođenja = (DateTime)reader["datum_rod"],
                        Spol = (string)reader["Spol"],
                        Objekt = (string)reader["vrtic"],
                        Skupina = (string)reader["skupina"],
                        Program = (string)reader["program"],
                        ŠkolskiObveznici = (string)reader["skolskiObveznik"],
                        Specifičnost = (string)reader["specificnost"],
                        Dokumentacija = (string)reader["dokumentacija"],
                        DokumentacijaUVrticu = (string)reader["dokumentacijaUVrticu"],
                        Upis = (string)reader["upis"],
                        StručniSuradnici = (string)reader["strucniSuradnici"]
                    };
                    polaznici.Add(polaznik);
                }
                return true;
            }
            catch (Exception ex)
            {
                logError += ex.Message + "\n";
            }
            return false;
        }

        public bool DeleteAllPolaznici()
        {

            try
            {
                if (state != ConnectionState.Open)
                {
                    if (!ConnectToDb() && state != ConnectionState.Open)
                    {
                        return false;
                    }
                }
                SQLiteCommand createTblCmd = new SQLiteCommand("DELETE FROM polaznici;)", _SQLiteConnection);
                createTblCmd.ExecuteNonQuery();
                return true;
            }
            catch (Exception ex)
            {
                logError += ex.Message + "\n";
            }
            return false;
        }

    }
}
