﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SQLite;
using System.IO;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace VrticApp2
{
    public class CSVManager
    {
        List<Polaznik> _polaznici;
        string _filename;
        public string logError;
        string[] _kolumne = { "OBJEKT", "IME I PREZIME", "DATUM ROĐENJA", "SPOL", "SKUPINA", "PROGRAM", "ŠKOLSKI OBVEZNIK", "SPECIFIČNOST", "DOKUMENTACIJA", "DOKUMENTACIJA U VRTIĆU", "UPIS", "STRUČNI SURADNICI"};
        Action<int> _incrementFunc;

        public CSVManager(string filename, ref List<Polaznik> polaznici, Action<int> incrementFunc = null)
        {
            _filename = filename;
            _polaznici = polaznici;
            _incrementFunc = incrementFunc;
            if(_incrementFunc == null)
            {
                _incrementFunc = delegate { };
            }
        }
        ~CSVManager()
        {
        }

        public bool readExcel()
        {
            bool retval = false;
            try
            {
                System.Globalization.CultureInfo oldCultureInfo = System.Threading.Thread.CurrentThread.CurrentCulture;
                System.Globalization.CultureInfo newCultureInfo = new System.Globalization.CultureInfo("en-US");
                System.Threading.Thread.CurrentThread.CurrentCulture = newCultureInfo;
                Excel.Application xlApp = new Excel.Application();
                Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(_filename);
                Excel.Worksheet xlWorksheet = (Excel.Worksheet)xlWorkbook.Sheets[1];
                Excel.Range xlRange = xlWorksheet.UsedRange;
                if (xlRange.Columns.Count != _kolumne.Length)
                {
                    logError += "Broj kolumna nije " + _kolumne.Length + ".\n" ;
                    retval = false;
                }
                else
                {
                    try
                    {
                        int tick = (int)Math.Ceiling(100.0 / xlRange.Rows.Count);
                        for (int i = 1; i <= xlRange.Rows.Count; ++i)
                        {
                            Polaznik polaznik = new Polaznik();
                            if (i == 1)
                            {
                                // imena kolumni
                                continue;
                            }
                            List<string> polaznikDetails = new List<string>();
                            for (int j = 1; j <= _kolumne.Length; ++j)
                            {
                                if (xlRange.Cells[i, j] != null && xlRange.Cells[i, j] != null)
                                {
                                    Excel.Range cell = (xlRange.Cells[i, j] as Excel.Range);
                                    if (j == 3)
                                    {
                                        DateTime? dt = (cell.Value as DateTime?);
                                        if (dt != null)
                                        {
                                            polaznikDetails.Add(string.Format("{0}.{1}.{2}.", dt.Value.Day, dt.Value.Month, dt.Value.Year));
                                        }
                                        else
                                        {
                                            polaznikDetails.Add(cell.Value.ToString());
                                        }
                                    }
                                    else if(j == 12 && cell.Value == null)
                                    {
                                        polaznikDetails.Add("");
                                    }
                                    else
                                    {
                                        polaznikDetails.Add(cell.Value.ToString());
                                    }
                                }
                            }
                            polaznik = getPolaznik(polaznikDetails.ToArray());
                            _polaznici.Add(polaznik);
                            _incrementFunc(tick);
                        }
                        retval = true;
                    }
                    catch (Exception ex)
                    {
                        logError += "Neočekivana greška kod čitanja Excel-a:" + ex.Message + "\n";

                    }
                }
                GC.Collect();
                GC.WaitForPendingFinalizers();

                //rule of thumb for releasing com objects:
                //  never use two dots, all COM objects must be referenced and released individually
                //  ex: [somthing].[something].[something] is bad

                //release com objects to fully kill excel process from running in the background
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);

                //close and release
                xlWorkbook.Close();
                Marshal.ReleaseComObject(xlWorkbook);

                //quit and release
                xlApp.Quit();
                Marshal.ReleaseComObject(xlApp);
                System.Threading.Thread.CurrentThread.CurrentCulture = oldCultureInfo;
            }
            catch(Exception ex)
            {
                logError += "Neočekivana greška kod čitanja Excel-a:" + ex.Message + "\n";
                return false;
            }
            return retval;
        }

        public bool writeCSV()
        {
            try
            {
                StreamWriter stream = new StreamWriter(_filename, false, Encoding.UTF8);
                int tick = (int)Math.Ceiling(100.0 /_polaznici.Count);
                foreach (Polaznik polaznik in _polaznici)
                {
                    stream.WriteLine(polaznik.Objekt + ";"
                        + polaznik.Ime + (polaznik.Prezime.Trim().Length > 0 && polaznik.Prezime.Trim().Length > 0 ? (" " ) : "") + polaznik.Prezime +  ";"
                        + polaznik.DatumRođenja.Day.ToString() + "." + polaznik.DatumRođenja.Month.ToString() + "." + polaznik.DatumRođenja.Year.ToString() + ";"
                        + (polaznik.Spol.ToString().Equals("Muški") ? "M" : polaznik.Spol.ToString().Equals("Ženski") ? "Ž" : "") + ";"
                        + polaznik.Skupina + ";"
                        + polaznik.Program + ";"
                        + polaznik.ŠkolskiObveznici + ";"
                        + polaznik.Specifičnost + ";"
                        + polaznik.Dokumentacija + ";"
                        + polaznik.DokumentacijaUVrticu + ";"
                        + (polaznik.Upis.ToString().Equals("Staro") ? "Staro" : polaznik.Upis.ToString().Equals("Novo") ? "Novo" : "Nedefinirano") + ";"
                        + polaznik.StručniSuradnici );
                    _incrementFunc(tick);
                }
                stream.Close();
                return true;
            }
            catch(Exception ex)
            {
                logError += "Neočekivana greška kod pisanja CSV-a:" + ex.Message + "\n";
            }
            return false;
        }

        public bool readCSV()
        {
            try
            {
                if(!processCSV())
                {
                    logError = "Dogodila se greška kod čitanja CSV-a.";
                }
                return true;
            }
            catch (Exception ex)
            {
                logError += "Neočekivana greška kod čitanja CSV-a:" + ex.Message + "\n";
            }
            return false;
        }

        bool processCSV()
        {
            if (_filename.Trim().Length != 0)
            {
                StreamReader stream = new StreamReader(_filename);
                string line;
                line = stream.ReadLine();
                _polaznici.Clear();
                while (stream.Peek() >= 0)
                {
                    line = stream.ReadLine();
                    string[] polaznikDetalji = line.Trim().Trim(';').Trim().Split(';');
                    if (polaznikDetalji.Length != _kolumne.Length)
                    {
                        logError += "Broj kolumna nije " + _kolumne.Length + ". Redak: " + line;
                        continue;
                    }
                    Polaznik polaznik = new Polaznik();
                    polaznik = getPolaznik(polaznikDetalji);
                    _polaznici.Add(polaznik);
                    _incrementFunc(1);
                }
                return true;
            }
            return false;
        }

        Polaznik getPolaznik(string[] polaznikDetalji)
        {
            Polaznik polaznik = new Polaznik();
            polaznik.Objekt = polaznikDetalji[0].Trim();
            string[] imeprezime = polaznikDetalji[1].Trim().Split(' ');
            polaznik.Ime = imeprezime[0].Trim();
            polaznik.Prezime = polaznikDetalji[1].Substring(imeprezime[0].Length).Trim();
            string[] dateStr = polaznikDetalji[2].Trim().Replace('/', '.').Trim('.').Trim().Split('.');
            if (dateStr.Length == 3)
            {
                polaznik.DatumRođenja = new DateTime(Int32.Parse(dateStr[2]), Int32.Parse(dateStr[1]), Int32.Parse(dateStr[0]));
            }
            polaznik.Spol = polaznikDetalji[3].Trim().ToUpper().Equals("Ž") ? Polaznik.spolovi[1] : polaznikDetalji[3].Trim().ToUpper().Equals("M") ? Polaznik.spolovi[0] : "";
            polaznik.Skupina = polaznikDetalji[4].Trim();
            polaznik.Program = polaznikDetalji[5].Trim();
            polaznik.ŠkolskiObveznici = polaznikDetalji[6].Trim();
            polaznik.Specifičnost = polaznikDetalji[7].Trim();
            polaznik.Dokumentacija = polaznikDetalji[8].Trim();
            polaznik.DokumentacijaUVrticu = polaznikDetalji[9].Trim();
            polaznik.Upis = polaznikDetalji[10].Trim().ToLower().Equals("staro") ? Polaznik.upisi[0] : polaznikDetalji[10].Trim().ToLower().Equals("novo") ? Polaznik.upisi[1] : "";
            polaznik.StručniSuradnici = polaznikDetalji[11].Trim();
            return polaznik;
        }
    }
}
