﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VrticApp2
{
    public enum Spol   
    {
        Nedefinirano,
        Muski,
        Zenski
    }

    public enum Upis
    {
        Nedefinirano,
        Staro,
        Novo
    }

    public class Polaznik
    {

        public static string[] objekti = { "Vrbik", "Cvjetno", "Poljane" };
        public static string[] spolovi = { "Muško", "Žensko" };
        public static string[] upisi = { "Staro", "Novo", "0 upis" };
        public static string[] programi = { "Redovni", "Montessori", "Engleski", "Sport", "Predškola" };
        public static string[] školskiObveznik = { "Redovni", "Odgoda", "Ne" };
        public static string[] skupine = { "Jaslice", "Vrtić" };
        public static string[] specificnosti = { "Bez specifičnost", "TUR", "PP", "ZT", "D" };
        public static string[] dokumentacije = { "BN", "Nalazi", "Kategorizacija" };
        public static string[] dokumentacijeUVrticu = { "BD", "Mišljenje", "IOOP" };
        public static string[] dobi = { "1-2", "2-3", "3-4", "4-5", "5-6", "6-7", "7-8" };
        public static string[] suradnici = { "PED", "PSIH", "LOG", "REH", "ZDRAV", "TO" };

        private string ime;
        private string prezime;
        private DateTime datumRođenja;
        private string spol;
        private string objekt;
        private string skupina;
        private string školskiObveznici;
        private string program;
        private string specificnost;
        private string upis;
        private string stručniSuradnici;
        private string dokumentacija;
        private string dokumentacijaUVrticu;

        public string Objekt { get => objekt; set => objekt = value; }
        public string Ime { get => ime; set => ime = value; }
        public string Prezime { get => prezime; set => prezime = value; }
        public string Dob { get => GetGodina() + "-" + (GetGodina() + 1); }
        public string Spol { get => spol; set => spol = value; }
        public string Skupina { get => skupina; set => skupina = value; }
        public string Program { get => program; set => program = value; }
        public string ŠkolskiObveznici { get => školskiObveznici; set => školskiObveznici = value; }
        public string Specifičnost { get => specificnost; set => specificnost = value; }
        public string Dokumentacija { get => dokumentacija; set => dokumentacija = value; }
        public string DokumentacijaUVrticu { get => dokumentacijaUVrticu; set => dokumentacijaUVrticu = value; }
        public string Upis { get => upis; set => upis = value; }
        public string StručniSuradnici { get => stručniSuradnici; set => stručniSuradnici = value; }
        public DateTime DatumRođenja { get => datumRođenja; set => datumRođenja = value; }

        private int GetGodina()
        {
            return DateTime.Now.Year - DatumRođenja.Year + (DateTime.Now.Month - DatumRođenja.Month > 0 ? 0 : (DateTime.Now.Month - DatumRođenja.Month == 0 && DateTime.Now.Day - DatumRođenja.Day >= 0 ? 0 : -1));
        }
    }
}
