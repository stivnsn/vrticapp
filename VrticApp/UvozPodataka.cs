﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace VrticApp2
{
    public partial class UvozPodataka : Form
    {
        VrticDBManager _dbManager;
        public UvozPodataka(VrticDBManager dbManager)
        {
            InitializeComponent();
            _dbManager = dbManager;
        }

        private void UcitajBtn_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Excel files (*.xlsx)|*.xlsx|CSV files (*.csv)|*.csv";
            ofd.ShowDialog();
            string CSVFileName = ofd.FileName;
            List<Polaznik> polaznici = new List<Polaznik>();
            CSVManager csvReader = new CSVManager(CSVFileName, ref polaznici, delegate (int a) { tickProgress(a); });
            if (CSVFileName.EndsWith(".csv"))
            {
                csvReader.readCSV();
            }
            else if (CSVFileName.EndsWith(".xlsx"))
            {
                csvReader.readExcel();
            }
            else
            {
                return;
            }
            ucitaniDGV.DataSource = polaznici;
            if (polaznici.Count > 0)
            {
                uveziBtn.Enabled = true;
            }
            progressControl1.Hide();

        }

        private void tickProgress(int tick)
        {
            progressControl1.increaseTick(tick);
        }

        private void uveziBtn_Click(object sender, EventArgs e)
        {
            progressControl1.show();
            List<Polaznik> stariPolaznici = new List<Polaznik>();
            _dbManager.GetPolaznici(ref stariPolaznici);
            //compare with database and insert if needed
            Action<int> action = progressControl1.increaseTick;
            _dbManager.SetPolaznici((List<Polaznik>)ucitaniDGV.DataSource, delegate(int a) { tickProgress(a); });
            progressControl1.Hide();
            this.Close();
        }

        private void odustaniBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
