﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;

namespace VrticApp2
{
    public partial class progressControl : UserControl
    {
        public progressControl()
        {
            InitializeComponent();
        }

        public void increaseTick(int tick)
        {
            progresPb.Increment(tick);
        }

        public void show()
        {
            progresPb.Value = 0;
            this.Show();
        }
    }


}
